import React, { Component } from 'react';
import { DRAWING_TOOLS } from './toolsConstants';

import { connect } from 'react-redux';
import * as actions from '../actions';

import Tool from './Tool';
import ColorPicker from './ColorPicker';

class DrawingTools extends Component{
    render(){
        const tools = Object.keys(DRAWING_TOOLS).map((property, inx) => {
            return <Tool key={inx} property={property} />
        });
        
        return(
            <div className="tools">
                <div className="drawing">
                    { tools }
                </div>
                <div className="color-panel">
                    <ColorPicker />
                </div>
            </div>
        )
    }


    shouldComponentUpdate(nextProps){
        return this.props.state.tool !== nextProps.state.tool || 
                    this.props.state.color !== nextProps.state.color
    }
}

export default connect(state => ({state}), actions) (DrawingTools);