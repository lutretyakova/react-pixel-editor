import { HEIGHT, WIDTH } from "../../editorConstants";

export const getFillArea = (board, point) => {
    const fillArea = [point];

    let coordsForCheck = [point];
    const neighbor = [
        [-1, 0], [1, 0], [0, -1], [0, 1]
    ]

    while(coordsForCheck.length > 0){
        let current = coordsForCheck.pop();
        for(let [dy, dx] of neighbor){
            if(current[0] + dy >= 0 && current[0] + dy < HEIGHT &&
                current[1] + dx >= 0 && current[1] + dx < WIDTH){
                    if (!hasPoint(fillArea, [current[0] + dy, current[1] + dx]) &&
                        board[current[0] + dy][current[1] + dx] === board[point[0]][point[1]]){
                        coordsForCheck.push([
                            current[0] + dy,
                            current[1] + dx
                        ]);
                        fillArea.push([
                            current[0] + dy,
                            current[1] + dx
                        ])
                    }
                }
        }
    }
    return fillArea
}


const hasPoint = (arrayForCheck, point) => {
    return arrayForCheck.filter((item) => (
        item[0] === point[0] && item[1] === point[1]
    )).length > 0
}