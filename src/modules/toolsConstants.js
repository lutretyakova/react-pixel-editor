export const PENCIL = 'pencil';

export const ERASER = 'eraser';

export const RECTANGLE = 'rectangle';

export const LINE = 'line';

export const CIRCLE = 'circle';

export const FILL = 'fill';

export const DRAWING_TOOLS = {
    [PENCIL]: 'fas fa-pencil-alt',
    [ERASER]: 'fas fa-eraser',
    [RECTANGLE]: 'far fa-square',
    [CIRCLE]: 'far fa-circle',
    [LINE]: 'fas fa-pencil-ruler',
    [FILL]: 'fas fa-fill-drip'
};

export const TRANSPARENT = 'transparent';