import React, { Component } from 'react';
import { PREVIEW_WIDTH, PREVIEW_HEIGHT, WIDTH, HEIGHT } from '../editorConstants';


class AdditionsTools extends Component{
      render(){
        const styleCanvas = {
            width: `${ PREVIEW_WIDTH }px`
        }
        const link = 'https://gitlab.com/lutretyakova/react-pixel-editor';
        return(
            <div className="additional-tools">
                <canvas ref="preview" id="preview" 
                    style={styleCanvas} width={ PREVIEW_WIDTH } height={ PREVIEW_HEIGHT }>
                </canvas>
                <div className="info">[ {WIDTH} x {HEIGHT} ]</div>
                <div>
                    <canvas ref="origin" id="origin" width={ WIDTH } height={ HEIGHT}></canvas>
                    <a className="tool-btn big" download="img.png" onClick={(e)=> this.handleSaveClick(e)}>
                        <i className="far fa-save"></i>
                    </a>
                    <div className="tool-btn big info"  data-descr={`Ссылка на исходный код \n ${link}`}>
                        <a href={link}><i className="fas fa-info"></i></a>
                    </div>
                </div>
            </div>
        )
    }


    handleSaveClick(e){
        const originIcon = this.refs.origin;
        const originCtx = originIcon.getContext('2d');
        originCtx.drawImage(this.refs.preview, 0, 0, WIDTH, HEIGHT);
        
        var dt = originIcon.toDataURL('image/png');    

        e.target.href = dt;
    }
}

export default AdditionsTools;