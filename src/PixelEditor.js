import React, { Component } from 'react';
import './PixelEditor.css';

import DrawingTools from './modules/DrawingTools';
import Canvas from './modules/Canvas';
import AdditionsTools from './modules/AdditionalTools';

class PixelEditor extends Component {
    render() {
        return (
            <div className="app">
                <div>
                    <header>
                        <h1>Pixel<span>Editor</span></h1>
                    </header>
                </div>
                <div className="page">
                    <DrawingTools />
                    <Canvas />
                    <AdditionsTools />
                </div>
            </div>
        );
    }
}

export default PixelEditor;
